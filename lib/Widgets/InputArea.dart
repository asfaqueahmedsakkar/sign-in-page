import 'package:flutter/material.dart';

class InputArea extends StatefulWidget {
  final String name;
  final IconData icon;
  final Function(String) onTextChanged;

  InputArea({this.name, this.icon, @required this.onTextChanged});

  @override
  InputAreaState createState() {
    return new InputAreaState();
  }
}

class InputAreaState extends State<InputArea> {
  TextEditingController _controller;

  @override
  void initState() {
    super.initState();
    _controller = new TextEditingController();

    _controller.addListener(() {
      widget.onTextChanged(_controller.text.toString());
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(widget.name),
            Icon(
              widget.icon,
              size: 18.0,
            ),
          ],
        ),
        SizedBox(
          height: 6.0,
        ),
        Container(
          decoration: BoxDecoration(
            color: Colors.white54,
            borderRadius: BorderRadius.all(Radius.circular(6.0)),
          ),
          child: TextField(
            controller: _controller,
            decoration: InputDecoration(
              focusedBorder: OutlineInputBorder(borderSide: BorderSide.none),
              contentPadding: EdgeInsets.all(12.0),
              border: OutlineInputBorder(borderSide: BorderSide.none),
              enabledBorder: OutlineInputBorder(borderSide: BorderSide.none),
              fillColor: Colors.black,
            ),
          ),
        ),
        SizedBox(
          height: 6.0,
        ),
      ],
    );
  }
}
