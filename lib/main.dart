import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_prototype/Widgets/Button.dart';
import 'package:flutter_prototype/Widgets/CircularButton.dart';
import 'package:flutter_prototype/Widgets/InputArea.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: AppInterfacePage(),
    );
  }
}

class AppInterfacePage extends StatefulWidget {
  @override
  _AppInterfacePageState createState() => _AppInterfacePageState();
}

class _AppInterfacePageState extends State<AppInterfacePage>
    with SingleTickerProviderStateMixin {
  StreamController _streamController;
  AnimationController _animationController;

  String _name, _password, _email, _confirmPassword;

  Function _onSubmit;

  @override
  void initState() {
    super.initState();

    _onSubmit = _signIn;

    _streamController = new StreamController.broadcast();
    _animationController = new AnimationController(
        vsync: this, duration: Duration(milliseconds: 300));
  }

  @override
  void dispose() {
    _streamController.close();
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.lightBlueAccent,
      child: Row(
        children: <Widget>[
          _signInSignUpButtons(),
          _indicator(),
          _signInSignUpForm()
        ],
      ),
    );
  }

  Widget _signInSignUpButtons() {
    return Container(
      width: 90.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Container(),
          ),
          Button(
            0,
            () {
              _onSubmit = _signIn;
              _animationController.reverse();
              _streamController.add(0);
            },
            child: Text(
              "Sign In",
              style: TextStyle(
                fontSize: 16.0,
                fontWeight: FontWeight.w600,
                color: Colors.white,
              ),
            ),
            stream: _streamController.stream,
          ),
          Button(
            1,
            () {
              _onSubmit = _signUp;
              _animationController.forward();
              _streamController.add(1);
            },
            stream: _streamController.stream,
            child: Text(
              "Sign Up",
              style: TextStyle(
                fontSize: 16.0,
                fontWeight: FontWeight.w600,
                color: Colors.white,
              ),
            ),
          ),
          Expanded(
            child: Container(
              width: 90.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  CircularButton(
                    _googleLogin,
                    size: 50.0,
                    child: Text(
                      "G",
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.w600,
                        color: Colors.red,
                      ),
                    ),
                  ),
                  CircularButton(
                    _facebookLogin,
                    size: 50.0,
                    child: Text(
                      "f",
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.w600,
                        color: Colors.blue,
                      ),
                    ),
                  ),
                  CircularButton(
                    _twitterLogin,
                    size: 50.0,
                    child: Text(
                      "t",
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.w600,
                        color: Colors.lightBlueAccent,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _indicator() {
    return Container(
      alignment: Alignment.centerRight,
      width: 34.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Container(),
          ),
          Container(
            alignment: Alignment.topRight,
            height: 140.0,
            child: AnimatedBuilder(
              animation: _animationController,
              builder: (context, child) {
                return Transform.translate(
                  child: Transform.rotate(
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.tealAccent,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.15),
                            spreadRadius: 8.0,
                            blurRadius: 12.0,
                          ),
                        ],
                      ),
                      height: 50.0,
                      width: 50.0,
                    ),
                    angle: pi * 0.25,
                  ),
                  offset: Offset(24.0, 5.0 + 70 * _animationController.value),
                );
              },
            ),
          ),
          Expanded(
            child: Container(),
          ),
        ],
      ),
    );
  }

  Widget _signInSignUpForm() {
    return Expanded(
      child: Container(
        height: 470.0,
        alignment: Alignment.center,
        child: Material(
          color: Colors.transparent,
          child: Stack(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(16.0),
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.only(bottom: 20.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.horizontal(
                    left: Radius.circular(30.0),
                    right: Radius.circular(0.0),
                  ),
                  color: Colors.tealAccent,
                  boxShadow: [
                    BoxShadow(
                      offset: Offset(20.0, 0.0),
                      color: Colors.black.withOpacity(0.15),
                      spreadRadius: 8.0,
                      blurRadius: 12.0,
                    ),
                  ],
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    _formLabel(),
                    Expanded(
                      flex: 2,
                      child: AnimatedBuilder(
                        animation: _animationController,
                        builder: (context, child) {
                          return Transform.translate(
                            offset: Offset(
                                200.0 - 200.0 * _animationController.value,
                                0.0),
                            child: Opacity(
                              opacity: _animationController.value,
                              child: child,
                            ),
                          );
                        },
                        child: InputArea(
                          name: "User Name",
                          icon: Icons.person,
                          onTextChanged: (value) {
                            _name = value;
                          },
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: InputArea(
                        name: "Email",
                        icon: Icons.email,
                        onTextChanged: (value) {
                          _email = value;
                        },
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: InputArea(
                        name: "Password",
                        icon: Icons.build,
                        onTextChanged: (value) {
                          _password = value;
                        },
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: AnimatedBuilder(
                        animation: _animationController,
                        builder: (context, child) {
                          return Transform.translate(
                            offset: Offset(
                                200.0 - 200.0 * _animationController.value,
                                0.0),
                            child: Opacity(
                              opacity: _animationController.value,
                              child: child,
                            ),
                          );
                        },
                        child: InputArea(
                          name: "Confirm Password",
                          icon: Icons.build,
                          onTextChanged: (value) {
                            _confirmPassword = value;
                          },
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(),
                    ),
                  ],
                ),
              ),
              _formSubmissionButton()
            ],
          ),
        ),
      ),
    );
  }

  Widget _formLabel() {
    return Expanded(
      flex: 2,
      child: Container(
        alignment: Alignment.centerLeft,
        child: AnimatedBuilder(
          animation: _animationController,
          builder: (context, child) {
            return Stack(
              children: <Widget>[
                Transform.translate(
                  child: Opacity(
                    child: Text(
                      "Sign In",
                      style: TextStyle(fontSize: 34.0),
                    ),
                    opacity: 1 - _animationController.value,
                  ),
                  offset: Offset(
                    0.0,
                    15 * _animationController.value,
                  ),
                ),
                Transform.translate(
                  child: Opacity(
                    child: Text(
                      "Sign Up",
                      style: TextStyle(fontSize: 34.0),
                    ),
                    opacity: _animationController.value,
                  ),
                  offset: Offset(
                    0.0,
                    -15 + 15 * _animationController.value,
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  Widget _formSubmissionButton() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        height: 50.0,
        width: 150.0,
        child: Card(
          elevation: 12.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              25.0,
            ),
          ),
          child: RawMaterialButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(
                25.0,
              ),
            ),
            padding: EdgeInsets.all(0.0),
            onPressed: () {
              _onSubmit();
            },
            child: AnimatedBuilder(
              animation: _animationController,
              builder: (context, child) {
                return Stack(
                  children: <Widget>[
                    Transform.translate(
                      child: Opacity(
                        child: Text("Sign In"),
                        opacity: 1 - _animationController.value,
                      ),
                      offset: Offset(
                        0.0,
                        15 * _animationController.value,
                      ),
                    ),
                    Transform.translate(
                      child: Opacity(
                        child: Text("Sign Up"),
                        opacity: _animationController.value,
                      ),
                      offset: Offset(
                        0.0,
                        -15 + 15 * _animationController.value,
                      ),
                    ),
                  ],
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  void _signUp() {
    print("Sign Up");
  }

  void _signIn() {
    print("Sign In");
  }

  void _facebookLogin() {
    print("Facebook login");
  }

  void _twitterLogin() {
    print("Twitter login");
  }

  void _googleLogin() {
    print("Google login");
  }
}
