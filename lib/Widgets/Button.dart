import 'dart:async';

import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  final Stream stream;

  final onPress;
  final Widget child;

  final size, elevation;

  final id;

  Button(
    this.id,
    this.onPress, {
    this.child,
    this.size = 70.0,
    this.elevation = 12.0,
    this.stream,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: size,
      padding: EdgeInsets.only(
        left: 0.0,
        right: 8.0,
        top: 13.0,
        bottom: 13.0,
      ),
      child: StreamBuilder(
        stream: stream,
        initialData: 0,
        builder: (context, snapshot) {
          return Card(
            margin: EdgeInsets.all(0.0),
            elevation: elevation,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.horizontal(
                left: Radius.circular(0.0),
                right: Radius.circular(size / 2),
              ),
            ),
            color: snapshot.data == id ? Colors.redAccent : Colors.blueAccent,
            child: RawMaterialButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.horizontal(
                  left: Radius.circular(0.0),
                  right: Radius.circular(size / 2),
                ),
              ),
              onPressed: onPress ?? () {},
              child: Container(
                child: child,
              ),
            ),
          );
        },
      ),
    );
  }
}
