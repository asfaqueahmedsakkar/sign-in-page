import 'package:flutter/material.dart';

class CircularButton extends StatelessWidget {
  final onPress;
  final Widget child;

  final size, elevation;

  CircularButton(this.onPress,
      {this.child, this.size = 70.0, this.elevation = 12.0});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 8.0),
      height: size,
      width: size,
      child: Card(
        elevation: elevation,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(size / 2),
        ),
        child: RawMaterialButton(
          onPressed: onPress ?? () {},
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(size / 2),
          ),
          child: Container(
            child: child,
          ),
        ),
      ),
    );
  }
}
